from behave import *
from selenium.webdriver.common.by import By

from tests import helpers

type = "Bug"
description = "This is a bug"
addressLine = "ul. Rzeczypospolitej"
cityLine = "Gdansk"
user_email = "test1@icm"
user_password = "Test123!"


@given('icm app is working')
def step_impl(context):
    driver = context.driver
    driver.get('http://localhost:9998?language=en')
    assert driver.find_element_by_link_text('ICM')


@given('user is logged in')
def step_impl(context):
    driver = context.driver
    helpers.logout_if_logged_in(driver)
    helpers.login(driver)
    assert helpers.is_element_present(driver, By.LINK_TEXT, "Logout") == True


@when('user fills all fields on new incident dialog')
def step_impl(context):
    driver = context.driver
    # Task 1 Put your code which opens new incident dialog, fills all fields and clicks Utwórz


@then('incident is created')
def step_impl(context):
    driver = context.driver
    assert helpers.is_element_present(driver, By.ID, "alert") == True
    id = driver.find_element_by_id('alert').find_element_by_tag_name('span').text
    assert id.isdigit()
    context.incident_id = id


@then('incident can be viewed')
def step_impl(context):
    incident_id = context.incident_id
    driver = context.driver
    # Task 2 Put code here which opens created incident page and asserts for an element on the page
