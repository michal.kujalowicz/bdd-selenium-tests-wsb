Feature: creating an incident

  Scenario: completing new incident dialog creates an incident
    Given icm app is working
    And user is logged in
    When user fills all fields on new incident dialog
    Then incident is created
    And incident can be viewed