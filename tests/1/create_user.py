from behave import *
import os

from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from tests import config  # import global properties
from selenium import webdriver
from webdriver_manager.firefox import GeckoDriverManager
from webdriver_manager.chrome import ChromeDriverManager

base_url = "http://localhost:9998?language=en"
clean_db_url = "http://localhost:9998/setup?action=do"

user_name = "test1"
user_email = "test1@icm"
user_password = "Test123!"

@given('clean icm app is working')
def step_impl(context):

    driver = "firefox"
    # Setting up webdriver and adding it to the context
    if driver == "firefox":
        profile = webdriver.FirefoxProfile()
        profile.set_preference('browser.download.folderList', 2)
        profile.set_preference('browser.download.manager.showWhenStarting', False)
        profile.set_preference('browser.download.dir', os.path.abspath(config.TMP_DIR))
        profile.set_preference('browser.helperApps.neverAsk.saveToDisk', 'text/csv, text/txt, application/vnd.ms-excel')
        profile.set_preference('general.warnOnAboutConfig', False)
        driver = webdriver.Firefox(executable_path=GeckoDriverManager().install(), firefox_profile=profile)
    if driver == "chrome":
        # driver = webdriver.Chrome()
        options = ChromeOptions()
        # the path must be absolute, otherwise Chrome won't download the file
        options.add_experimental_option("prefs", {"download.default_directory": os.path.abspath(config.TMP_DIR)})
        driver = webdriver.Chrome(executable_path=ChromeDriverManager().install(), chrome_options=options)

    driver.implicitly_wait(1)
    driver.maximize_window()
    context.driver = driver

    # Checking if ICM application is started
    driver.get(base_url)
    assert driver.find_element_by_link_text('ICM')

    # Reseting database
    driver.get(clean_db_url)

@when('we create a user')
def step_impl(context):
    # Getting webdriver from the context
    driver = context.driver

    # Opening and filling new user form
    driver.find_element_by_xpath("//a[@href='/signup']").click()
    driver.find_element_by_id("name").send_keys(user_name)
    driver.find_element_by_id("email").send_keys(user_email)
    driver.find_element_by_id("password").send_keys(user_password)
    driver.find_element_by_id("confirmedPassword").send_keys(user_password)
    driver.find_element_by_id("signup").click()

@then('user can log in from scratch')
def step_impl(context):
    # Getting webdriver from the context
    driver = context.driver

    # Logging in after logging out
    logout_if_logged_in(driver)
    login(driver)

    # Checking if user is logged in
    assert is_element_present(driver, By.LINK_TEXT, "Logout") == True

    context.driver.close()

def is_element_present(driver, how, what):
    try:
        driver.find_element(by=how, value=what)
    except NoSuchElementException:
        return False
    return True


def logout_if_logged_in(driver):
    if (is_element_present(driver, By.LINK_TEXT, "Logout")):
        driver.find_element_by_xpath("//a[@href='/logout']").click()


def login(driver):
    driver.find_element_by_xpath("//a[@href='/signin']").click()
    driver.find_element_by_id("inputEmail").clear()
    driver.find_element_by_id("inputEmail").send_keys(user_email)
    driver.find_element_by_id("inputPassword").clear()
    driver.find_element_by_id("inputPassword").send_keys(user_password)

    driver.find_element_by_id("signin").click()


