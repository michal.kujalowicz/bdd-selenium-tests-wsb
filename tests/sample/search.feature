Feature: User is able to perform search

  Scenario: User can search for Horoskop
    Given trojmiasto.pl is avaiable
    When user searches for Horoskop
    Then Horoskop for 2020 is returned
    And Horoskop can be opened
