Feature: creating an incident

  Scenario: logged in user sees create incident button
    Given icm app is working
    When user logs in
    Then create incident button is visible
