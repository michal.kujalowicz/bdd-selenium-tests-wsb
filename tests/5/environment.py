from tests import config
from tests import helpers


def before_all(context):

    # Setting up webdriver and adding it to the context
    driver = helpers.new_driver(driver="firefox", implicitly_wait=0.25)
    context.driver = driver

    #  Clean database
    driver.get(config.CLEAN_DB_URL)

    # Create user to use for tests
    helpers.create_user(driver)


def after_all(context):
    driver = context.driver
    driver.close()




