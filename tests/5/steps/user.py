from behave import *
from selenium.webdriver.common.by import By

from tests import helpers
from tests import config


@given('user is logged in')
def step_impl(context):
    driver = context.driver
    helpers.logout_if_logged_in(driver)
    helpers.login(driver)
    assert helpers.is_element_present(driver, By.LINK_TEXT, "Logout") == True


@when('user logs in')
def step_impl(context):
    driver = context.driver
    helpers.logout_if_logged_in(driver)
    helpers.login(driver)
    assert helpers.is_element_present(driver, By.LINK_TEXT, "Logout") == True


@when('we create a regular user')
def step_impl(context):
    driver = context.driver
    driver.find_element_by_xpath("//a[@href='/signup']").click()
    driver.find_element_by_id("name").send_keys(config.USER_NAME)
    driver.find_element_by_id("email").send_keys(config.USER_EMAIL)
    driver.find_element_by_id("password").send_keys(config.USER_PASSWORD)
    driver.find_element_by_id("confirmedPassword").send_keys(config.USER_PASSWORD)
    driver.find_element_by_id("signup").click()


@then('user is automatically logged in')
def step_impl(context):

    driver = context.driver
    assert helpers.is_element_present(driver, By.LINK_TEXT, "Logout") == True


@then('user can log in from scratch')
def step_impl(context):
    driver = context.driver
    helpers.logout_if_logged_in(driver)
    helpers.login(driver)
    assert helpers.is_element_present(driver, By.LINK_TEXT, "Logout") == True

