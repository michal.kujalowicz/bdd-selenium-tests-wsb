from behave import *
from selenium.webdriver.common.by import By
from tests import helpers
from tests import config

type = "Bug"
long_type = "012345678901234567890123456789012345678901234567890"
description = "This is a bug"
addressLine = "ul. Rzeczypospolitej"
cityLine = "Gdansk"


@then('create incident button is visible')
def step_impl(context):
    driver = context.driver
    assert helpers.is_element_present(driver, By.LINK_TEXT, "Create incident") == True


@when('user fills all fields on new incident dialog')
def step_impl(context):
    driver = context.driver
    driver.find_element_by_xpath("//a[@href='/incident/create']").click()
    driver.find_element_by_id("type").send_keys(type)
    driver.find_element_by_id("description").send_keys(description)
    driver.find_element_by_id("addressLine").send_keys(addressLine)
    driver.find_element_by_id("cityLine").send_keys(cityLine)
    driver.find_element_by_id("create").click()


@then('incident is created')
def step_impl(context):
    driver = context.driver
    assert helpers.is_element_present(driver, By.ID, "alert") == True
    id = driver.find_element_by_id('alert').find_element_by_tag_name('span').text
    assert id.isdigit()
    context.incident_id = id


@then('incident can be viewed')
def step_impl(context):
    incident_id = context.incident_id
    driver = context.driver
    driver.get(config.APP_URL + 'incident/' + incident_id + "?language=en")
    text = driver.find_element_by_xpath("//h2").text
    assert text == "Details"


@when('user does not fill all fields on new incident dialog')
def step_impl(context):
    driver = context.driver
    driver.find_element_by_xpath("//a[@href='/incident/create']").click()
    driver.find_element_by_id("type").send_keys(type)
    driver.find_element_by_id("description").send_keys(description)
    driver.find_element_by_id("addressLine").send_keys(addressLine)
    driver.find_element_by_id("create").click()


@then('incident is not created')
def step_impl(context):
    driver = context.driver
    alert = driver.find_element_by_id("alert").text
    assert alert == "Could not create new incident"
